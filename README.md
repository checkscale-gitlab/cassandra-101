# Cassandra 101 Playground

The following repositories contains a complete infrastructure for performing reproducible experiments with Apache Cassandra database.

**What you can do**:

- examine communication between nodes in Cassandra Cluster,
- learn how the data is being distributed (using `nodetool`),
- see how to use a client plugin to exchange data (Node.js API)

Infrastructure is provisioned with Docker 1.9, and Docker-compose. You can get both from [here](https://www.docker.com/).

## Spin-up a stack
**TLDR**:

    docker-compose up

**Long version**:

A [docker-compose file](docker-compose.yml) describes two Apache Cassandra (version 3.3) containers and a custom client app. Everything is running within separated network created by Docker (also defined in the file).

A configuration of cluster nodes is done accordingly to this [link](https://hub.docker.com/_/cassandra/) and stored within environment variables. Please note that you should use it only for the dev-puroposes. Notice that Cassandra is quite memory-consuming (everything takes about 8 GB of RAM on my machine).

The client app is able to locate cluster's contact points by specifying container names (due to networking feature). It also exposes one port outside. If you're using Docker Machine remember to change the IP address to access it.

## Provision Cassandra Database

When everything is fired and working it's time to provision the database with initial keyspace and table.

You can access Cassandra CQL shell in 2 ways:

- using binary installed on host,
- connecting to one of the nodes terminal and starting `cqlsh`

When using a second option - start a new terminal. To access running container in interactive mode type:

    docker exec -ti cassandra_1 bash
    cqlsh

You should now have access to CQL shell. To create and use an exemplary keyspace paste the following code:

    CREATE KEYSPACE pp_ex_keyspace
    WITH REPLICATION = {'class': 'SimpleStrategy', 'replication_factor': 1};

    USE pp_ex_keyspace;

Let's also create a table that will hold information about users:

    CREATE TABLE users (
      user_id uuid,
      name text,
      group int,
      PRIMARY KEY ((group), name, user_id)
    );

The data will be partitioned using `group` value. It means that each cluster should have it's own portion of data based on the hash of `group` (distribution is done using token ring). To ensure uniqueness  of each row there are two extra *clustering columns* - `name` and `user_id`.

Insert some dummy data:

    INSERT INTO users(user_id, name, group) VALUES (uuid(), 'Mark', 1);
    INSERT INTO users(user_id, name, group) VALUES (uuid(), 'Bob', 1);
    INSERT INTO users(user_id, name, group) VALUES (uuid(), 'Karen', 2);

And verify if all works correctly (you can even switch container):

    SELECT * FROM users;

Queries are also stored [here](bootstrap.sql).

## Experiment

It's a little awkward to perform operations directly in shell. Let's try to use an extra layer of the stack - [client app](app/). It's a tiny, little script written in Javascript being able to collect and insert data.

### Requests

To collect all user data (must query for data from all nodes):

    curl -XGET localhost:3000/all

To collect data only from specific partition (only coordinator and 1 node is involved).

    curl -XGET localhost:3000/group/1

To add new row:

    curl -XPOST -H "Content-Type:application/json" localhost:3000/new -d '{"name":"Captain", "group":1}'
